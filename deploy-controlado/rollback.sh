#!/bin/bash

# Definição de diretórios
vhost="/var/vhost/horadosalao-front"
cd $vhost/repository/rollback
# Obtenção do diretório de backup
backup_dir=$(./ultimo_rollback.sh)
echo "Diretório de backup obtido: $backup_dir"
cd $vhost/repository
# Função para restaurar backup
restore_backup() {
    echo "Restaurando do último backup..."
    
    # Verifica se o diretório de backup existe
    if [[ ! -d "$backup_dir" ]]; then
        echo "Diretório de backup não encontrado: $backup_dir"
        return 1
    fi
    
    # Assegura que o diretório de destino esteja limpo antes de restaurar
    if [[ -d "$vhost/public" ]]; then
        rm -rf "$vhost/public/"*
        echo "Diretório público limpo."
    else
        echo "Diretório público não encontrado: $vhost/public"
        return 1
    fi

    # Copia o conteúdo do backup para o diretório público
    cp -r "$backup_dir/"* "$vhost/public/"
    echo "Backup restaurado para o diretório público."
    return 0
}

# Execução da restauração e tratamento de erros
if restore_backup; then
    echo "Rollback concluído com sucesso."
else
    echo "Erro ao realizar o rollback"
    exit 1
fi