#!/bin/bash

# Definição de caminhos
vhost="/var/vhost/horadosalao-front"

# Garantindo que os diretórios existem
if [[ ! -d "$vhost/repository/rollback" ]]; then
    echo "Diretório de rollback não encontrado."
    exit 1
fi

# Navega até o diretório de rollback
cd "$vhost/repository/rollback/" || exit

# Executa script para criar o diretório de rollback
./criadiretorio_rollback.sh

# Captura o nome do diretório do último rollback
diretorioroll=$(./ultimo_rollback.sh)
if [[ -z "$diretorioroll" ]]; then
    echo "Não foi possível obter o diretório de rollback."
    exit 1
fi

echo "Diretório de rollback: $diretorioroll"

# Copia os conteúdos para o novo diretório de rollback
if [[ -d "$vhost/public" ]]; then
    cp -r "$vhost/public/"* "$diretorioroll/"
else
    echo "Diretório público não encontrado."
    exit 1
fi

# Remove conteúdo do diretório público
rm -rf "$vhost/public/"*

# Navega até o repositório do front-end
if [[ -d "$vhost/repository/horadosalao-front" ]]; then
    cd "$vhost/repository/horadosalao-front" || exit
else
    echo "Repositório do front-end não encontrado."
    exit 1
fi

# Atualiza o repositório
git pull origin main

# Verifica se o diretório de distribuição existe
if [[ -d "$vhost/repository/horadosalao-front/dist/vex" ]]; then
    # Copia o conteúdo para o diretório público
    cp -r "$vhost/repository/horadosalao-front/dist/vex/"* "$vhost/public/"
else
    echo "Diretório de distribuição não encontrado."
    exit 1
fi