#!/bin/bash

# Configura o trap para capturar erros
trap 'echo "{\"error\":\"An error occurred\"}" >&2; exit 1' ERR

# Lista diretórios, os ordena e gera JSON
find "$(pwd)" -maxdepth 1 -type d -printf '%T+ %p\n' -name "rollback_*" | sort -r 

if rollbacks=$(find "$(pwd)" -maxdepth 1 -type d -name "rollback_*" -printf $(pwd)/'%P\n' | sort -r | awk '{print NR "," $0}' | jq -R -s 'split("\n") | map(select(length > 0)) | map({"id": (split(",")[0] | tonumber), "path": (split(",")[1])})'); then
    echo $rollbacks | jq
else
    echo "{\"error\":\"Failed to generate JSON\"}" >&2
    exit 1
fi
