#!/bin/bash
# Remove diretórios antigos, mantendo os três mais recentes, suprimindo a saída
find "$(pwd)" -maxdepth 1 -type d -name "rollback_*" -printf '%T+ %p\n' 2>/dev/null | sort -r | tail -n +4 | cut -d' ' -f 2- | xargs rm -rv 2>/dev/null
./lista_full_rollbacks.sh