#!/bin/bash

# Define a função para gerar o JSON do diretório mais novo
lista_rollback_mais_novo_json() {
    # Encontra o diretório mais novo e gera a saída JSON
    vrollback=$(find "$(pwd)" -maxdepth 1 -type d -name "rollback_*" -printf '%T@ %P\n' | sort -nr | head -n 1 | awk '{print "1," $2}')
    if [[ -z "$vrollback" ]]; then
        echo "{\"error\":\"No rollback directories found\"}"
        return 1
    fi
    echo "$vrollback" | jq -R -s 'split("\n") | map(select(. != "")) | map({"id": (split(",")[0] | tonumber), "path": (split(",")[1])})'
}

# Gera o JSON do diretório mais novo de rollback
json=$(lista_rollback_mais_novo_json)

# Verifica se o JSON foi gerado corretamente e o exibe
if [ $? -eq 0 ]; then
    path=$(echo "$json" |jq -r '.[0].path')
    echo $(pwd)/$path
else
    echo "{\"error\":\"Failed to generate JSON\"}" >&2
    exit 1
fi
