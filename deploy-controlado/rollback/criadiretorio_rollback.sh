#!/bin/bash

# Configura o trap para capturar erros
trap 'echo "{\"error\":\"An error occurred\"}" >&2; exit 1' ERR

# Armazena a data e hora atual no formato 'DD-MMM-YYYY-HHMMSS' em uma variável
data=$(date +%d-%b-%Y-%H%M%S)

# Define o caminho completo do diretório a ser criado
dir_path="$(pwd)/rollback_$data"

# Cria o diretório com o nome baseado na data e hora atuais, suprimindo a saída
mkdir -pv "$dir_path" 2>/dev/null

# Verifica se o diretório foi criado com sucesso e imprime o caminho
if [[ -d "$dir_path" ]]; then
    echo "$dir_path"
else
    echo "{\"error\":\"Failed to create directory\"}" >&2
    exit 1
fi
